﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace awad
{
    interface IWorker
    {
        void Execute(HttpListenerContext context);
    }
}
