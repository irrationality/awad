﻿using System;
using System.Net;
using System.Text;


namespace awad
{
    /// <summary>
    /// Обработчика запроса прокси
    /// </summary>
    class ProxyLogic : IWorker
    {
        /// <summary>
        /// Обработать запрос
        /// </summary>
        public async void Execute(HttpListenerContext context)
        {
            var result = "Обрабатываются только GET  запросы";
            if (context.Request.HttpMethod == "GET")
            {
                var url = context.Request.QueryString["url"];

                if (url != null)
                {
                    try
                    {
                        var uri = new Uri(url);
                        var request = (HttpWebRequest)WebRequest.Create(uri);
                        using (var response = await request.GetResponseAsync())
                        {
                            using (var respStream = response.GetResponseStream())
                            {
                                var buffer = new byte[512];
                                int bytesRead;
                                while ((bytesRead = respStream.Read(buffer, 0, buffer.Length)) > 0)
                                {
                                    context.Response.OutputStream.Write(buffer, 0, bytesRead);
                                }
                                context.Response.StatusCode = 200;
                                context.Response.KeepAlive = false;
                                context.Response.OutputStream.Close();
                                context.Response.Close();
                                return;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        var errMessage = Encoding.UTF8.GetBytes(ex.Message);
                        context.Response.StatusCode = 200;
                        context.Response.ContentLength64 = errMessage.Length;
                        context.Response.KeepAlive = false;
                        context.Response.OutputStream.Write(errMessage, 0,errMessage.Length);
                        context.Response.OutputStream.Close();
                        context.Response.Close();
                        return;
                    }
                }

                result = "Не указан параметр url";
            }

            var b = Encoding.UTF8.GetBytes(result);
            context.Response.ContentLength64 = b.Length;
            context.Response.KeepAlive = false;
            context.Response.StatusCode = 200;
            context.Response.OutputStream.Write(b, 0, b.Length);
            context.Response.OutputStream.Close();
            context.Response.Close();
        }

    }
}
