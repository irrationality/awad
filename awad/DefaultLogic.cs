﻿using System.Net;
using System.Text;


namespace awad
{
    class DefaultLogic : IWorker
    {
        public void Execute(HttpListenerContext context)
        {
            var hellWorld = Encoding.UTF8.GetBytes("Hello world");
            context.Response.StatusCode = 200;
            context.Response.KeepAlive = false;
            context.Response.ContentLength64 = hellWorld.Length;

            var output = context.Response.OutputStream;
            output.Write(hellWorld, 0, hellWorld.Length);
            output.Close();
            context.Response.Close();
        }

    }
}
