﻿using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Runtime.Remoting.Lifetime;


namespace awad
{
    /// <summary>
    /// Обработчик хранилища sqlite
    /// </summary>
    public class SqliteLogic
    {
        private const string DbName = "guestBook.db";
        private static readonly object Sync = new object();

        public SqliteLogic()
        {
            // Если db файл создан не создаем его заново
            if (File.Exists(DbName)) return;

            // для thread safety локаем этот код
            lock (Sync)
            {
                if (File.Exists(DbName)) return;

                SQLiteConnection.CreateFile(DbName);
                using (var connection = new SQLiteConnection(string.Format("Data Source={0};", DbName)))
                {
                    var createUsersTableCommand = new SQLiteCommand("CREATE TABLE users (id INTEGER PRIMARY KEY, value TEXT);", connection);
                    var createMessagesTableCommand = new SQLiteCommand("CREATE TABLE messages (id INTEGER PRIMARY KEY,userId INTEGER, value TEXT);", connection);
                    
                    connection.Open();

                    createUsersTableCommand.ExecuteNonQuery();
                    createMessagesTableCommand.ExecuteNonQuery();

                    connection.Close();
                }
            }
        }

        /// <summary>
        /// Добавить запись
        /// </summary>
        public void AddRecord(string user, string message)
        {
            using (var connection = new SQLiteConnection(string.Format("Data Source={0};", DbName)))
            {
                var addUserCommand = new SQLiteCommand(
                    string.Format("INSERT INTO 'users' ('id', 'value') VALUES (null , '{0}');", user),
                    connection);

                var addMessageCommand = new SQLiteCommand(
                   string.Format("INSERT INTO 'messages' ('id','userId', 'value') VALUES (null ,  (SELECT max(id) FROM users), '{0}');", message),
                   connection);

                connection.Open();
                addUserCommand.ExecuteNonQuery();
                addMessageCommand.ExecuteNonQuery();
                connection.Close();
            }

        }

        /// <summary>
        /// Получить записи смапленные в структуру записи
        /// </summary>
        public List<GuestBookRecord> GetRecords()
        {
            var result = new List<GuestBookRecord>();

            using (var connection = new SQLiteConnection(string.Format("Data Source={0};", DbName)))
            {
                var getGuestBookCommand = new SQLiteCommand(
                   "SELECT u.value, m.value FROM 'users' u INNER JOIN 'records' r on u.id = r.userId INNER JOIN 'messages' m on m.Id = r.messageId;",
                   connection);
                connection.Open();
                using (var reader = getGuestBookCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        result.Add(new GuestBookRecord
                        {
                            User = reader.GetString(0),
                            Message = reader.GetString(1)
                        });
                    }
                }
                connection.Close();
            }

            return result;
        }

    }
}
