﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace awad
{
    /// <summary>
    /// Класс обработчик запросов логики гостевой книги (использует хранилищи xml/sqlite для внесения и получения  записей)
    /// </summary>
    public class GuestBookLogic : IWorker
    {
        private readonly string _storageMode = ConfigurationManager.AppSettings["storage"] ?? "xml";
        private readonly object _sync =  new Object();
       
        /// <summary>
        /// Выполнить обработку
        /// </summary>
        public void Execute(HttpListenerContext context)
        {
            var output = context.Response.OutputStream;
            try
            {
                var result = "Обрабатываются только GET и POST запросы";
                if (context.Request.HttpMethod == "GET")
                {
                    result = GetGuestBook();

                }

                if (context.Request.HttpMethod == "POST")
                {
                    result = context.Request.ContentType == "application/json"
                        ? AddRecord(context)
                        : "Обрабатываются запросы только c ContentType = application/json. Example data:  {\"user\": \"username\", \"message\": \"hi awad\"} ";
                }

                var b = Encoding.UTF8.GetBytes(result);
                context.Response.StatusCode = 200;
                context.Response.KeepAlive = false;
                context.Response.ContentLength64 = b.Length;
                output.Write(b, 0, b.Length);
                
            }
            catch(Exception ex)
            {
                var b = Encoding.UTF8.GetBytes(ex.Message);
                context.Response.StatusCode = 200;
                context.Response.KeepAlive = false;
                context.Response.ContentLength64 = b.Length;
                output.Write(b, 0, b.Length);
            }

            output.Close();
            context.Response.Close();
        }

        /// <summary>
        /// Прокси получения гостевой книги 
        /// </summary>
        private string GetGuestBook()
        {
            switch (_storageMode)
            {
                case "xml":
                    return GetFromXml();
                case "sqlite":
                    return GetFromSqlite();
                default:
                    return "";
            }
        }

        /// <summary>
        /// Получить из гостевую книгу из xml
        /// </summary>
        private static string GetFromXml()
        {
            try
            {
                var xml = XDocument.Load("GuestBookStorage.xml");
                return xml.Root.ToString();
            }
            catch (Exception ex)
            {
                return string.Format("ошибка загрузки гостевой книги из xml. {0}", ex.Message);
            }
        }


        /// <summary>
        /// Получить гостевую книгу из sqlite
        /// </summary>
        private static string GetFromSqlite()
        {
            try
            {
                var sqlite = new SqliteLogic();
                var records =  sqlite.GetRecords();
                
                return JsonConvert.SerializeObject(records);
            }
            catch (Exception ex)
            {
                return string.Format("ошибка загрузки гостевой книги из xml. {0}", ex.Message);
            }

        }

        /// <summary>
        /// Прокси добавления
        /// </summary>
        private string AddRecord(HttpListenerContext context)
        {
            switch (_storageMode)
            {
                case "xml":
                    return AddToXml(context);

                case "sqlite":
                    return AddToSqlite(context);
                default:
                    return "";
            }

        }

        /// <summary>
        /// Добавить в xml
        /// </summary>
        private string AddToXml(HttpListenerContext context)
        {
            lock (_sync)
            {
                if (!File.Exists("GuestBookStorage.xml"))
                {
                    var newXml = new XDocument();
                    newXml.Add(new XElement("guestBook"));
                    newXml.Save("GuestBookStorage.xml");
                }

                try
                {
                    var body = new StreamReader(context.Request.InputStream).ReadToEnd();
                    var data = JsonConvert.DeserializeObject<GuestBookRecord>(body);
                    var xml = XDocument.Load("GuestBookStorage.xml");

                    xml.Root.Add(new XElement("record",
                            new XAttribute("user", data.User),
                            new XAttribute("message", data.Message)));
                    xml.Save("GuestBookStorage.xml");

                    return JsonConvert.SerializeObject(new { result = "OK" });
                }
                catch (Exception exc)
                {
                    return JsonConvert.SerializeObject(new { result = exc.Message });
                }
          
              
            }
        }

        /// <summary>
        /// Добавилть в sqlite
        /// </summary>
        private static  string AddToSqlite(HttpListenerContext context)
        {
            try
            {
                var body = new StreamReader(context.Request.InputStream).ReadToEnd();
                var data = JsonConvert.DeserializeObject<GuestBookRecord>(body);

                var sqlite = new SqliteLogic();
                sqlite.AddRecord(data.User,data.Message);
              
                return JsonConvert.SerializeObject(new {result = "OK"});
            }
            catch (Exception exc)
            {
                return JsonConvert.SerializeObject(new {result = exc.Message});
            }
          
        }
    }
}

/// <summary>
/// Структура записи гостевой книги 
/// </summary>
public struct GuestBookRecord
{
    public string User { get; set; }
    public string Message { get; set; }
}
