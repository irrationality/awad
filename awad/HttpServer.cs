﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Policy;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace awad
{
    /// <summary>
    /// Реализация http сервера с некоторой логикой (запрос прокси, запросы гостевой книги, все остальные запросы выводим Hello World) 
    /// </summary>
    public class HttpServer : IDisposable
    {
       
        public bool KeepProcessing { get; set; }

        private readonly HttpListener _listener = new HttpListener();
        /// <summary>
        /// Cловарь для инстанцирования обработчиков запросов 
        /// </summary>
        private Dictionary<string, Type> _workers; 
        
        public HttpServer()
        {
            InitWorkers();
            var port = ConfigurationManager.AppSettings["port"] ?? "9999";
            
            _listener.Prefixes.Add(string.Format("http://*:{0}/", port));
            _listener.Start();
            
        }

        public void Start()
        {
            KeepProcessing = true;
            ProccessRequests(_listener);
        }

        public void Dispose()
        {
         
        }


        /// <summary>
        /// Цикл получения запросов
        /// </summary>
        private async void ProccessRequests(HttpListener listener)
        {
            while (KeepProcessing)
            {
                var context = await listener.GetContextAsync();
                ProccessContext(context);
            }

            listener.Stop();
        }


        /// <summary>
        /// Асинхронная ообработка запроса
        /// </summary>
        private  void ProccessContext(HttpListenerContext context)
        {
           Task.Factory.StartNew(() =>
            {
                LogRequest(context.Request);
                var worker = GetWorker(context.Request);
                worker.Execute(context);
            });
           
        }

        /// <summary>
        /// Получить обработчик
        /// </summary>
        private IWorker GetWorker(HttpListenerRequest request)
        {
            var rawUrl = request.RawUrl.ToLower();

            var worker = _workers.Where(wrk =>
            {
                // регуляркой матчик из урла ключ обработчика для его инстанцирования и обработки запроса
                var regex = new Regex(string.Format("^/{0}", wrk.Key));
                return regex.IsMatch(rawUrl);
            }).Select(wrk=>wrk.Value).FirstOrDefault();

                                            // Используем reflection для инстанцирования обработчиков
            return worker != null ? (IWorker)Activator.CreateInstance(worker) : new DefaultLogic();
        }

        /// <summary>
        /// Пишем лог запросов в консоль
        /// </summary>
        private void LogRequest(HttpListenerRequest request)
        {
            Console.WriteLine("incoming request HttpMethod = {0}, url = {1}, userAgent = {2}",
                   request.HttpMethod,
                   request.Url,
                   request.UserAgent
                   );
            Console.WriteLine();
        }


        /// <summary>
        /// Инициализация словаря обработчиков
        /// </summary>
        private void InitWorkers()
        {
            _workers = new Dictionary<string, Type>()
            {
                {"guestbook", Type.GetType("awad.GuestBookLogic")},
                {"proxy", Type.GetType("awad.ProxyLogic")}
            };
        }

    }
}
